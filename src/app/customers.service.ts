import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customersCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  getCustomers(userId): Observable<any[]> {
    this.customersCollection = this.db.collection(`users/${userId}/customers`, 
       ref => ref.limit(10))
    return this.customersCollection.snapshotChanges();    
  }

  addCustomer(userId:string,name:string,education:number,income:number){
    const customer = {name:name, education:education, income:income};
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

  deleteCustomer(userId:string,customerId:string){
    this.db.doc(`users/${userId}/customers/${customerId}`).delete();
  }

  updateCustomer(userId:string,customerId:string,name:string,education:number,income:number){
    this.db.doc(`users/${userId}/customers/${customerId}`).update(
      {
        name:name,
        education:education,
        income:income,
        result:''
      }
    )
  }

  updateResult(userId:string,customerId:string,result:string){
    this.db.doc(`users/${userId}/customers/${customerId}`).update(
      {
        result:result
      }
    )
  }

  constructor(private db:AngularFirestore) { }
}
