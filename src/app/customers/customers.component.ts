import { Customer } from './../interfaces/customer';
import { CustomersService } from './../customers.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictService } from '../predict.service';


@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId:string;
  customers$;
  hiddenCreate:boolean = true;
  hiddenEdit = [];

  customers:Customer[];
  result:string;

  displayedColumns: string[] = ['name', 'education', 'income', 'update', 'delete', 'predict', 'result'];

  add(customer:Customer){
    this.customersService.addCustomer(this.userId,customer.name,customer.education,customer.income);
  }

  delete(customerId:string){
    this.customersService.deleteCustomer(this.userId,customerId);
  }

  update(customer:Customer){
    this.customersService.updateCustomer(this.userId,customer.id,customer.name,customer.education,customer.income);
  }

  sendResult(i,id,result){
    this.customersService.updateResult(this.userId,id,result);
  }

  sendData(i,education:number,income:number){
    this.predictService.predict(education,income).subscribe(
      res => {console.log(res);
        this.result = res;
        if (res > 0.5){
          var result ='will pay';
        } else {
          var result ='will not pay';
        }
        this.customers[i].result = result;
      }
    );
  }
  constructor(private customersService:CustomersService, public authService:AuthService, private predictService:PredictService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              for (let document of docs) {
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true;
                }
                customer.id = document.payload.doc.id;
                this.customers.push(customer); 
              }                        
            }
          )
      })
 }
  }

