import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = 'https://fg3y5ic6i8.execute-api.us-east-1.amazonaws.com/test';

  predict(education:number, income:number):Observable<any>{
    let json = {
      "data": 
        {
          "education": education,
          "income": income
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private http:HttpClient) { }
}
