import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  notAllowedEdu:boolean;

  @Input() formType:string;
  @Input() name:string;
  @Input() education:number;
  @Input() income:number;
  @Input() customerId:string;

  @Output() update = new EventEmitter<Customer>();
  @Output() closeForm = new EventEmitter<null>();

  updateParent(){
    let customer:Customer = {id:this.customerId,name:this.name, education:this.education, income:this.income};
    if(this.education < 0 || this.education > 24){
      this.notAllowedEdu = true;
    }else{
      this.update.emit(customer);
      if(this.formType == "Add customer"){
        this.name = null;
        this.education = null;
        this.income = null;
      }
    }
  }

  tellParentToClose(){
    this.closeForm.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
