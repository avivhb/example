// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCA96NkHqE0czSQ_VfcR2f4KuU1f2_rcOw",
    authDomain: "example-33c31.firebaseapp.com",
    projectId: "example-33c31",
    storageBucket: "example-33c31.appspot.com",
    messagingSenderId: "607597111096",
    appId: "1:607597111096:web:6dfc72c86c9e26ca5103cd"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
